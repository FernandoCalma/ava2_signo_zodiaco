#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include <string.h>
#include "menu.h"
#include "criarArquivoFrase.h"

#define TAM_MATRIZ_ZODIACO 12
#define TAM_TEXTO 100

struct fraseHoroscopo {
    int indexador;
    char fraseMotivacional[500];
};

void montarArquivoComFrases(void)
{
    char *matrizSignoZodiaco[TAM_MATRIZ_ZODIACO][3] =
    {
        {"11","22/12 a 19/01","Capric�rnio"},
        {"12","20/01 a 18/02","Asqu�rio"},
        {"13","19/02 a 20/03","Peixes"},
        {"14","21/03 a 19/04","�ries"},
        {"15","20/04 a 20/05","Touro"},
        {"16","21/05 a 21/06","G�meos"},
        {"17","22/06 a 22/07","C�ncer"},
        {"18","23/07 a 22/08","Le�o"},
        {"19","23/08 a 22/09","Virgem"},
        {"20","23/09 a 22/10","Libra"},
        {"21","23/10 a 21/11","Escorpi�o"},
        {"22","22/11 a 21/12","Sagit�rio"}
    };
    int contador;
    setlocale(LC_ALL,"");

    FILE *arquivoHoroscopo;

    for(contador = 0; contador <= TAM_MATRIZ_ZODIACO-1 ; contador++)
    {
        int textoValido = 0;
        if (contador == 0)
            arquivoHoroscopo = fopen("horoscopo.txt", "w");
        else
            arquivoHoroscopo = fopen("horoscopo.txt", "a");

        do {

            int index = atoi(matrizSignoZodiaco[contador][0]);
            struct fraseHoroscopo frase = {index};
            system("cls");
            printf("\n\n\t\t*** CRIAR ARQUIVOS COM FRASES MOTIVACIONAIS***\n\n\n\n");
            printf("Entre com a frase motivacional para o signo de %s - %s", matrizSignoZodiaco[contador][2], matrizSignoZodiaco[contador][1]);
            printf("\n\n [----------------------------------- Tamanho m�ximo de 100 posi��es -----------------------------------]\n");
            scanf("%s", frase.fraseMotivacional);
            if (strlen(frase.fraseMotivacional) < 100) {
              fprintf(arquivoHoroscopo, "%d%s\n", frase.indexador, frase.fraseMotivacional);
              textoValido = 1;
            }
        }while (textoValido == 0);

        //fputs(frase, arquivoHoroscopo);
        fclose(arquivoHoroscopo);
        printf("\nDados gravados com sucesso!");

    }
    printf("\n");
    system("pause");
    return;
}
