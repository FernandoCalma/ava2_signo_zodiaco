#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include "descobrirSigno.h"

void descobrirSigno(void)
{
    char *matrizSignoZodiaco[][3] =
    {
        {"11","22/12 a 19/01","Capric�rnio"},
        {"12","20/01 a 18/02","Asqu�rio"},
        {"13","19/02 a 20/03","Peixes"},
        {"14","21/03 a 19/04","�ries"},
        {"15","20/04 a 20/05","Touro"},
        {"16","21/05 a 21/06","G�meos"},
        {"17","22/06 a 22/07","C�ncer"},
        {"18","23/07 a 22/08","Le�o"},
        {"19","23/08 a 22/09","Virgem"},
        {"20","23/09 a 22/10","Libra"},
        {"21","23/10 a 21/11","Escorpi�o"},
        {"22","22/11 a 21/12","Sagit�rio"}
    };

    int mesDia , indexadorMensagem = 100;
    char buffer[] = "1234", dia[3], mes[3];
    setlocale(LC_ALL,"");
    system("cls");
    printf("\t\t *** INFORMA O SIGNO *** ");
    printf("\n\n\n Informe o dia e o m�s que voc� nasceu (DDMM): ");
    scanf("%s", buffer);

    memcpy(dia, &buffer[0], 2);
    dia[2] = '\0'; // adiciona o terminador de linha
    //printf("%s", dia);

    memcpy(mes, &buffer[2], 2);
    mes[2] = '\0'; // adiciona o terminador de linha
    //printf("%s", mes);


    strcat(mes,dia);
    mesDia = atoi(mes);
    //printf("%d", mesDia);

    if (mesDia < 101) {
        indexadorMensagem = 100;
    }else if ((mesDia >= 101) && (mesDia <= 119)) {
        indexadorMensagem = 0;
    }else if ((mesDia >= 120) && (mesDia <= 218)) {
        indexadorMensagem = 1;
    }else if ((mesDia >= 219) && (mesDia <= 320)) {
        indexadorMensagem = 2;
    }else if ((mesDia >= 321) && (mesDia <= 419)) {
        indexadorMensagem = 3;
    }else if ((mesDia >= 420) && (mesDia <= 520)) {
        indexadorMensagem = 4;
    }else if ((mesDia >= 512) && (mesDia <= 621)) {
        indexadorMensagem = 5;
    }else if ((mesDia >= 622) && (mesDia <= 722)) {
        indexadorMensagem = 6;
    }else if ((mesDia >= 723) && (mesDia <= 822)) {
        indexadorMensagem = 7;
    }else if ((mesDia >= 823) && (mesDia <= 922)) {
        indexadorMensagem = 8;
    }else if ((mesDia >= 923) && (mesDia <= 1022)) {
        indexadorMensagem = 9;
    }else if ((mesDia >= 1023) && (mesDia <= 1121)) {
        indexadorMensagem = 10;
    }else if ((mesDia >= 1122) && (mesDia <= 1221)) {
        indexadorMensagem = 11;
    }else if ((mesDia >= 1221) && (mesDia <= 1231)) {
        indexadorMensagem = 12;
    } else{
        indexadorMensagem = 100;
    }

    if (indexadorMensagem < 100) {
        printf(" ==> Voc� � do signo de ***\t %s - %s \n\n", matrizSignoZodiaco[indexadorMensagem][2],matrizSignoZodiaco[indexadorMensagem][1]);
    }else{
        printf("\t\t *** DATA INVALIDA *** \n\n");
        system("pause");
        descobrirSigno();
    }

    system("pause");
}
