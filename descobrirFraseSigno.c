#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include <string.h>
#include "menu.h"
#include "descobrirFraseSigno.h"

#define TAM_MATRIZ_ZODIACO 12
#define TAM_TEXTO 100

void descobrirFraseSigno(void)
{
    setlocale(LC_ALL,"");
    int indexadorSigno, contador = 0;
    char *matrizSignoZodiaco[TAM_MATRIZ_ZODIACO][3] =
    {
        {"11","22/12 a 19/01","Capric�rnio"},
        {"12","20/01 a 18/02","Asqu�rio"},
        {"13","19/02 a 20/03","Peixes"},
        {"14","21/03 a 19/04","�ries"},
        {"15","20/04 a 20/05","Touro"},
        {"16","21/05 a 21/06","G�meos"},
        {"17","22/06 a 22/07","C�ncer"},
        {"18","23/07 a 22/08","Le�o"},
        {"19","23/08 a 22/09","Virgem"},
        {"20","23/09 a 22/10","Libra"},
        {"21","23/10 a 21/11","Escorpi�o"},
        {"22","22/11 a 21/12","Sagit�rio"}
    }, fraseMotivacional[TAM_TEXTO];

    FILE *arquivoHoroscopo;
    arquivoHoroscopo = fopen("horoscopo.txt", "r");

    char frase[TAM_TEXTO];
    system("cls");

    printf("\n\n\t\t\t\t\t *** FRASE MOTIVACIONAL DO SIGNO *** \n\n\n\n");
    printf("\t\t *--------------------------------------------------------------------------------------*\n ");
    printf("\t\t ! \t\t\t\t Signos do zodiacos \t\t\t\t\t!\n ");

    printf("\t\t ! \t(1)Capric�rnio \t(2)Asqu�rio \t\t(3)Peixes \t(4)�ries \t\t!\n");
    printf("\t\t ! \t(5)Touro \t(6)G�meos \t\t(7)C�ncer \t(8)Le�o \t\t!\n");
    printf("\t\t ! \t(9)Virgem \t(10)Libra \t\t(11)Escorpi�o \t(12)Sagit�rio \t\t!\n");

    printf("\t\t *--------------------------------------------------------------------------------------* ");


    printf("\n\n Digite o n�mero que representa o seu signo: ");
    scanf("%d", &indexadorSigno);

    if ((indexadorSigno >= 1) && (indexadorSigno <= 12)){
        while(fgets(frase, 100, arquivoHoroscopo) != NULL){
            if (contador == indexadorSigno-1) {
                memcpy(fraseMotivacional, &frase[2], 100);
                fraseMotivacional[99] = '\0'; // adiciona o terminador de linha
                printf("\n\n\nFrase: %s \n\n\n", &fraseMotivacional);
            }
            contador++;
        }
        fclose(arquivoHoroscopo);
        system("pause");
    }else{
        fclose(arquivoHoroscopo);
        descobrirFraseSigno();
    }
    return;
}
