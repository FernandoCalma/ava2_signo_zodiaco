# AV2_ signos_zodiacas

SIGNOS ZODIACAIS

Este trabalho tem por objetivo explorar os recursos apresentados na Unidade 4, 
trazendo a oportunidade de praticá-los através 
de um programa que deve ser escrito em linguagem C.

 

O programa é sobre os signos zodiacais e possui um menu inicial 
onde uma das opções é a criação de um arquivo com frases motivacionais 
associadas aos signos. Após criadas, qualquer usuário pode 
informar o seu signo para conhecer a sua frase motivacional. 
Na dúvida sobre o signo, existe também uma opção que 
solicita o dia e o mês de aniversário e informa o signo correspondente.

A astrologia se baseia na posição em que se encontram os astros no céu no
momento do nascimento de uma pessoa, podendo influenciar o seu modo de ser. 
Para isso, existem doze signos do zodíaco: 
Capricórnio, Peixes, Aquário, Áries, Touro, Gêmeos, 
Câncer, Leão, Virgem, Libra, Escorpião e Sagitário. 

Cada um destes signos está associado a um período do ano, 
bastando saber o dia e mês de nascimento para 
conhecer a que signo a pessoa pertence.