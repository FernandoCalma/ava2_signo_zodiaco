#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "menu.h"
#include "criarArquivoFrase.h"
#include "descobrirSigno.h"
#include "descobrirFraseSigno.h"

void escolherOpcao(void)
{
    int opcaoMenu;
    setlocale(LC_ALL,"");
    system("cls");
    printf("\n\n----------- SIGNOS DO ZOD�ACO -----------\n\n\n");
    printf("\n");
    printf("0 - Cria arquivo com frases motivacionais\n");
    printf("1 - Saber qual � o signo\n");
    printf("2 - Frase motivacional do signo\n");
    printf("3 - Sair\n\n\n");
    printf("*** Entre com uma das op��es acima: ");
    scanf("%d", &opcaoMenu);

    switch(opcaoMenu)
     {
         case 0:
           printf("0");
           montarArquivoComFrases();
           escolherOpcao();
           break;
         case 1:
           printf("1");
           descobrirSigno();
           escolherOpcao();
           break;
         case 2:
           printf("2");
           descobrirFraseSigno();
           escolherOpcao();
           break;
         case 3:
            printf("Fim da Execu��o");
            return;
         default:
           system("cls");
           escolherOpcao();
    }

    return;
}
